/* jshint node:true,esnext:true */
let schemaCross = require('./schemaCross');
let yaml = require('js-yaml');
let cssColor = require('color');

let logger = require('logging').default('mb2mz');
logger.debug=logger.info;


function empty(obj) {
    return !obj || !Object.keys(obj).length;
}

/*
TODO:
✔ deal with dashes somehow? https://github.com/tangrams/tangram/issues/393
✔ opacity - https://mapzen.com/documentation/tangram/tutorials/custom-styles/
-- set blend:overlay, work opacity as alpha into color specs.
- line-gap-width: could maybe fake with 'outline' but it's complicated
- line-translate: not supported in Tangram
- fill-pattern: could maybe be supported through textures
✔ support 'ref' layers
✔ find out why dasharray isn't working
-- it's because the dash properties have to be a 'styles'...

- handle bold, italic etc
✔ Produce more idiomatic Tangram styles with "styles" and "layers" etc.
- find out why pois layer doesn't seem to contain anything. (Hence no park labels...)
*/


const unsupportedProps = /opacity|blur|line-gap-width|line-translate|text-line-height|text-letter-spacing|text-max-angle|antialias|fill-pattern/;

// Return a specific "not supported" message for a specific property, or undefined if it is (supposedly) supported.
function msgUnsupported(prop) {
    Object.keys(unsupportedProps).forEach(k => {
        if (prop.match(k)) {
            return unsupportedProps[k] || 'Not supported.';
        }
    });
}

function reportUnknownProps(mbprops, knownProps, mblayer) {
    let unknownProps = Object.keys(mbprops).filter(k => !knownProps.has(k) && !k.match(unsupportedProps));
    //console.log(unknownProps.length);
    if (unknownProps.length) {
        logger.warn(`[${mblayer.id}] Unhandled style properties: `, unknownProps.join(','));
    }
}

// https://mapzen.com/documentation/tangram/draw/
// Construct the 'draw' element, given a layer and its position in the layers array.
function walkStyle(mblayer, order) {
    // Mapbox layer types and Mapzen built-in styles aren't really the same thing but
    // they behave similarly. Not really sure where icons/sprites fit in.
    function walkGeometryType(mbtype) {
        // https://mapzen.com/documentation/tangram/Styles-Overview/#using-styles
        const type = {
            line: 'lines',
            fill: 'polygons',
            'fill-extrusion': 'polygons',
            circle: 'points', 
            symbol: 'text', // not really
            raster: 'raster' // not actually supporting this
        }[mbtype];
        if (type === undefined) {
            logger.error('Unrecognised layer type', mblayer);
        }
        return type;

    }

    // source: https://github.com/mapbox/mapbox-gl-js/blob/8909e261b303f3a8fd2579d80f97d339c15a35a9/src/style-spec/expression/definitions/curve.js
    function exponentialInterpolation(input, base, lowerValue, upperValue) {
        const difference = upperValue - lowerValue;
        const progress = input - lowerValue;

        if (difference === 0) {
            return 0;
        } else if (base === 1) {
            return progress / difference;
        } else {
            return (Math.pow(base, progress) - 1) / (Math.pow(base, difference) - 1);
        }
    }


    /* Convert a zoom-based property, possibly with exponential interpolation.
     { stops: [ [ 12, 2], [ 16, 6 ] ] } =>
     [ [ 12, 2px], [13, 3px]... [16, 6px] ]
    */
    // TODO property funcs and property-and-zoom funcs
    function walkStops(val, transform = x => x, noStops = false) {
        if (val === undefined) {
            return undefined;
        } else if (!Array.isArray(val.stops)) {
            return transform(val);
        } else if (noStops) {
            // a property type that Tangram doesn't support interpolating, we just pick one value.
            return transform(val.stops[0]);
        }
        //logger.info(val.stops);
        if (typeof val.stops[0][1] === 'number') { // TODO check for exponential, categorical...
            // exponentially interpolate
            let ret = [], zooms = val.stops.map(s => s[0]), vals = val.stops.map(s => s[1]);
            let base = val.base || 1; // Mapbox default
            // iterate over each of the explicitly provided stops
            for (let si = 0;  si < zooms.length - 1; si++) {
                const dz = 1; // Can reduce for more piece-wise approximations of the curve.
                // iterate over fixed intervals between stops.
                for (let z = zooms[si]; z < zooms[si+1]; z += dz) {
                    
                    let factor = exponentialInterpolation(z, base, zooms[si], zooms[si+1]);
                    let nextv = vals[si] + (vals[si+1] - vals[si]) * factor;
                    nextv = +(nextv).toFixed(2); // round to two decimal places
                    ret.push([z, transform(nextv)]);
                }
            }
            // Add the final stop.
            ret.push([zooms[zooms.length-1], transform(vals[vals.length-1])]);
            return ret;
        } else 
            return val.stops.map(stop => [ stop[0], transform(stop[1]) ]);
    }

    function setDrawDefaults(draw, type) {

        const propDefaults = {
            lines: [ [ 'width', '1px' ] ],
            polygons: [],
            points: [],
            text: [],
            raster: []
        };

        // We can't leave all properties undefined in Mapzen
        propDefaults[type].forEach(p => {
            if (draw[p[0]] === undefined) {
                draw[p[0]] = p[1];
            }
        });
        return draw;
    }

    let knownProps = new Set(); // all the properties that did map to something.

    /*
     Return props from mbprops converted through walk where walk is:
     {
        mbPropName: [ tangramPropName, transform ]
     }
    */
    function walkProps(mbprops, walk) {
        Object.keys(walk).forEach(k => knownProps.add(k));
        let ret = {};
        Object.keys(mbprops).forEach(mbprop => {
            let prop = walk[mbprop], propTransform;
            let noStops;
            if (!prop) {
                return;
            } else if (typeof prop === 'object') {
                propTransform = prop[1];
                noStops = prop[2];
                prop = prop[0];
            }
            ret[prop] = walkStops(mbprops[mbprop], propTransform, noStops);
        });    
        return ret;
    }

    // smoosh layout and paint props and hope for the best.
    let mbprops = mblayer.paint || {};
    Object.keys(mblayer.layout || {}).forEach(k => mbprops[k] = mblayer.layout[k]);

    // properties directly on the style object, not the draw object.
    let style = walkProps(mbprops, {
        'line-dasharray':           [ 'dash', x => x.map(d => Math.max(d,0.1)), true ]
    });

    //let px = x => x + 'px';
    let px = x => Array.isArray(x) ? x.map(px) : x + 'px';
    let draw = walkProps(mbprops, { 
        'line-cap':                   'cap', // butt, square round
        'line-join':                  'join', // bevel, round, miter
        'line-miter-limit':           'miter_limit',
        //line-round-limit
        'line-color':                 'color', 
        //line-translate, line-translate-anchor
        'line-width':               [ 'width', px ],
        //line-gap-width
        //line-offset
        //line-pattern
        'fill-color':                 'color' ,
        'fill-extrusion-color':       'color',

        'circle-radius':            [ 'size' , px ],
        'icon-size':                [ 'size', px], // unconfirmed
        'icon-image':                 'sprite',
        'icon-allow-overlap':       [ 'collide', x => !x ], // "collide" means "check for collisions"
        'icon-padding':             [ 'buffer', x => [x+'px', x+'px']],
        'icon-ignore-placement':    [ 'collide', x => !x ], // TODO check these subtleties
        'icon-rotation-alignment':  [ 'flat', x => x === 'map'],
        'icon-color':                 'color', // who knows if it works
        'icon-translate':           [ 'offset', px ],
        //icon-optional
        'symbol-placement':         [ 'placement', x => x === 'point' ? 'vertex' : 'spaced'],
        'symbol-spacing':           [ 'placement_spacing', px],
        //symbol-avoid-edges (move_into_tile doesn't help)
        'text-field':               [ 'text_source', t => t.replace(/[{}]/g, '').replace('name_en', 'name') ], // limited
        'text-max-width':             'wrap', // units: ems ~> characters
        'text-optional':              'optional',
        'text-offset':              [ 'offset', px ],
        'text-anchor':                'anchor', // the values, `top-left` etc seem to map ok
        'text-padding':             [ 'buffer', x => [x, x].map(px)],
        'text-allow-overlap':       [ 'collide', x => !x ],
        'text-rotation-alignment':  [ 'angle', x => x === 'viewport' ? 0 : 'auto'], // not sure if it applies
        'text-pitch-alignment':     [ 'flat', x => x === 'map' ],
        'text-justify':               'align', // left, center, right
        'visibility':               [ 'visible', x => x === 'visible']

    });



    // TODO do we need to transform Bold, Italic etc into specific props?
    let font = walkProps(mbprops, {
        'text-font':        [ 'family' ],
        'text-size':        [ 'size', px ],
        'text-transform':   [ 'transform' ],
        'text-color':       [ 'fill' ]
    });
    let fontStroke = walkProps(mbprops, {
        'text-halo-width': [ 'width' , px ],
        'text-halo-color': 'color'
    });

    if (!empty(font)) {
        draw.font = font;
        if (!empty(fontStroke)) {
            draw.font.stroke = fontStroke;
        }
        draw.font.size = draw.font.size || '14px'; 
    }

    let outline = walkProps(mbprops, {
        'fill-outline-color': 'color', 
        'icon-halo-color':    'color',
        'icon-halo-width':  [ 'width', px ]
    });
    if (!empty(outline)) {
        draw.outline = outline;
        draw.outline.width = draw.outline.width || '1px'; 
    }

    if (mblayer.type === 'fill-extrusion') {
        // TODO convert functions like 'identity' and 'interpolate' to a JS function
        draw.extrude = true;
    }


    const addOpacity = (color, opacity) => cssColor(color).alpha(opacity).string(); 

    // Opacity is only supported by using the fill-color's alpha channel and enabling blend mode.
    // Todo: support text-opacity, symbol-opacity, background-opacity
    ['fill','line','text', 'fill-extrusion'].forEach(styleType => {
        const opacity = mbprops[`${styleType}-opacity`];
        if (opacity) {
            let mbcolor = mbprops[`${styleType}-color`] || '#000000', color;
            // We do not correctly handle case with fill-opacity *and* an alpha channel in the color.
            if (typeof mbcolor !== 'string' && typeof opacity !== 'number') {
                logger.warn(mblayer.id, 'Opacity not supported when color and opacity are both functions.');
                return;
            } else if (typeof mbcolor !== 'string') {
                // Update the color function to include opacity everywhere.
                // it's a little bit grubby that we have already processed color once.
                color = walkStops(mbcolor, c => addOpacity(c, opacity));
            } else {
                // Create new color function based on opacity stops.
                color = walkStops(opacity, o => addOpacity(mbcolor, o));
            }
            if (styleType === 'text') {
                draw.font.fill = color;
            } else {
                draw.color = color;
            }
            style.blend = 'inlay';

        }
    });

    reportUnknownProps(mbprops, knownProps, mblayer);
    
    style.base = walkGeometryType(mblayer.type);

        // Sadly Tangram doesn't actually support this.
        //draw.visible = [ [0, false], [mblayer.minzoom, true], [mblayer.maxzoom, false]];


    // if (mblayer.minzoom && mblayer.maxzoom) {
    if (mblayer.minzoom) {
        if (draw.font && Array.isArray(draw.font.size)) {
            // this is a bodgy way of handling one particular edge case, with many assumptions
            draw.font.size = [ [mblayer.minzoom-0.1, '0px'], ...draw.font.size ];

        }
        //draw.visible = [ [0, false], [mblayer.minzoom, true]];
    } 
    if (mblayer.maxzoom) {
        if (draw.font && Array.isArray(draw.font.size)) {
            draw.font.size = [ ...draw.font.size, [mblayer.maxzoom+0.1, '0px']];
        }
    }

    draw = setDrawDefaults(draw, style.base);
    draw.interactive = true; // TODO remove, for debugging.
    draw.order = order;

    style.draw = draw;

    return style;

}

function walkStyles(mblayers) {
    let styles = {};
    mblayers.forEach((mblayer, i) => {
        styles[mblayer.id] = walkStyle(mblayer, i);
    });
    return styles;
}

// recursively convert one Mapbox layer filter
function walkFilter(mbfilter) {
    const geomtype = {
        Polygon: 'polygon',
        LineString: 'line',
        Point: 'point'
    };
    
    if (!mbfilter)
        return;
    
    let op = mbfilter[0], 
        key = mbfilter[1], 
        vals = mbfilter.slice(2)
            // not 100% sure about this. 
            .map(v => /^(true|false)$/.test(v) ? Boolean(v) : v),
        nonkey = mbfilter.slice(1);

    if (key === '$type') {
        key = '$geometry';
        vals = vals.map( v => geomtype[v] );
    }

    // TODO support >= and <= properly. Yuck.
    
    let f = {};
    const opFunc = {
        '=='   : _ => f[key] = vals[0],
        '!='   : _ => f.not  = { [key]: vals[0] }, 
        'in'   : _ => f[key] = vals,
        '!in'  : _ => f.not  = { [key]: vals },  
        'has'  : _ => f[key] = true,            
        '!has' : _ => f[key] = false,           
        '<'    : _ => f[key] = { max: vals[0] },
        '<='   : _ => f[key] = { max: vals[0] }, 
        '>'    : _ => f[key] = { min: vals[0] },
        '>='   : _ => f[key] = { min: vals[0] },
        'any'  : _ => f.any  = nonkey.map(walkFilter),
        'all'  : _ => f.all  = nonkey.map(walkFilter),
        'none' : _ => f.none = nonkey.map(walkFilter)
    }[op] || (_ => void logger.warn(`Unrecognised operator: ${op}`));
    //let filter = {};
    opFunc();
    return f;
}

// convert a whole slew of layers
function walkLayers(mblayers, mbsources) {
    let layers = {}, warnedZooms = false;
    // TODO deal with minzoom/maxzoom. it's tricky.
    mblayers.forEach(mblayer => {
        let layer;
        if (mblayer.ref) {
            // a "ref" in Mapbox means the data source is copied from another layer, but (I think) otherwise the styles
            // are independent. The equivalent in Tangram is (I think) a "sublayer" https://mapzen.com/documentation/tangram/layers/#sublayer-name
            // We create the sublayer with our ID under the ref layer, and create the two styles as normal.
            // Assumption: reffed style is defined before this one.
            if (mblayer.ref.match(/^(data|enabled|filter|draw)$/)) {
                logger.warn("Cross-reference to layer " + mblayer.ref + " which is a reserved word.");
            }
            console.debug(mblayer.ref, '->', mblayer.id);
            layer = layers[mblayer.ref][mblayer.id] = {};
        } else {
            layer = walkLayerSource(mblayer);
            layer.filter = walkFilter(mblayer.filter);
            layers[mblayer.id] = layer;
        }
        // The slightly odd we we reference a "style" draw block from within a "layer" draw block.
        // https://mapzen.com/documentation/tangram/styles/#draw
        layer.draw = { [mblayer.id]: {} }; 

        if (!warnedZooms && (mblayer.minzoom || mblayer.maxzoom)) {
            warnedZooms = true;
            logger.warn("Minzoom and maxzoom not supported");
        }        
    });
    return layers;
}

// convert 'background' type layer to a 'scene'
function walkBackground(mblayers) {
    const bgi = mblayers.findIndex(l => l.type === 'background');
    if (bgi === -1)
        return;

    return { 
        background: {
            color: mblayers[bgi].paint['background-color'] 
        }
    };        
}

function walkLayerSource(mblayer) {
    return {
        data: {
            source: 'mapfit',
            layer: mblayer['source-layer'] // mapping required!
            }
    };
}

// convert one whole Mapbox style to a Tangram style
// asYAML: return the output as string in YAML format, else a JavaScript object.
function walkStyleFile(mbstyle, asYAML, customLogger) {
    function copy(o) {
        return JSON.parse(JSON.stringify(o));
    }
    function filterLayers(l) {
        if (!l.type) {
            if (l.ref) {
                // we need to copy the ref layer's type for draw conversion to work.
                l.type = mbstyle.layers.find(mbl => mbl.id === l.ref).type;
            } else {
                return void logger.warn(`Ignoring layer ${l.id} as it has no type.`);
            }
        }
        return l.type !== 'background' //&& l.id.match('aeroway');
    }
    if (customLogger){
        logger = customLogger;
    }
    let filteredLayers = mbstyle.layers.filter(filterLayers);
    let ret = {
        sources: {
            mapfit: {
                type: 'MVT',
                url: 'http://tiles.mapfit.com/vector/v1/roads,water,places,buildings,landuse/{z}/{x}/{y}.mvt',
                tile_size: 512,
                max_zoom: 16
            }
        },
        scene: walkBackground(mbstyle.layers), 
        styles: walkStyles(filteredLayers),
        layers: walkLayers(filteredLayers, mbstyle.sources)
    };
    ret = copy(ret); // strip undefineds
    return asYAML ? yaml.safeDump(ret) : ret;
}

module.exports.toTangram = walkStyleFile;
